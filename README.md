# CRUD EMPLEADOS ANGULAR

Este proyecto fue generado con [Angular CLI](https://github.com/angular/angular-cli) version 15.2.2.

## Servidor de desarrollo

Run `ng serve --o` para el servidor de desarrollo. Navega hacia `http://localhost:4200/`. 

## Levantar el JSON Server 
run `json-server --watch db.json` para levantar el API. Navega hacia el `http://localhost:3000/empleados` para visualizar el API

## Ayuda

Para obtener mas informacion en Angular CLI utiliza `ng help` o revisa la pagina oficial  [Angular CLI Overview and Command Reference](https://angular.io/cli).

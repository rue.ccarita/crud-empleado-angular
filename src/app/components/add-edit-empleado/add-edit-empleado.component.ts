import { DialogRef } from '@angular/cdk/dialog';
import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material/dialog';
import { EmpleadoService } from 'src/app/services/empleado.service';
import { MensajeService } from 'src/app/services/mensaje.service';

@Component({
  selector: 'app-add-edit-empleado',
  templateUrl: './add-edit-empleado.component.html',
  styleUrls: ['./add-edit-empleado.component.scss']
})
export class AddEditEmpleadoComponent implements OnInit{
  empleadoForm:FormGroup;

  //Datos para el select del educacion
  educacion:string[]= [
    'Egresado',
    'Bachiller',
    'Magister',
    'Doctorado',
    'Licenciado'
  ]

  constructor(private _fb : FormBuilder,
    private _empleadoService:EmpleadoService,
    private _dialogRef:MatDialogRef<AddEditEmpleadoComponent>,
    private _mensajeService:MensajeService,
    @Inject(MAT_DIALOG_DATA) public data:any
    )
    
    {
      //aplicamos validator para validar que se ingrese los datos correctos
      this.empleadoForm = this._fb.group({
        nombres: ['', Validators.required],
        apellidos: ['', Validators.required],
        correo: ['', [Validators.required, Validators.email]],
        anio_nacimiento: ['', Validators.required],
        genero: ['', Validators.required],
        educacion: ['', Validators.required],
        empresa: ['', Validators.required],
        experiencia: ['', Validators.required],
        expectativa_salarial: ['', Validators.required],
      })
  }
  ngOnInit(): void {
    // El método patchValue() se utiliza para aplicar los valores del objeto this.data a los controles del formulario.
    this.empleadoForm.patchValue(this.data);
  }

  enviarFormularioEmpleado(){
    //primero verificamos si envia el formulario es valido
    if(this.empleadoForm.valid){
      //con este if entramos al edit, ya que abrira cuando hay data
      if(this.data){
        //se llama al servicio de actualizar y le pasamos el id, y los datos del empleados para actualizar
        this._empleadoService.actualizarEmpleado(this.data.id,this.empleadoForm.value)
        .subscribe({
          next:(res:any) =>{
            this._mensajeService.abrirSnackBar("Empleado Actualizado","Hecho")
            this._dialogRef.close(true);
          },
          error: (err:any) =>{
            console.log(err);
          }
        })
      }else{
        //cuando no hay data se abre la opcion normal de agregar datos
        this._empleadoService.addEmpleado(this.empleadoForm.value).subscribe({
          next:(res:any)=>{
              this._mensajeService.abrirSnackBar("Empleado Agregado Satisfactoriamente","Hecho")
              this._dialogRef.close(true);
          },
          error: (err) =>{
            console.log(err)
          }
        })
      }
      //console.log(this.empleadoForm.value)
      
    }
  }

}

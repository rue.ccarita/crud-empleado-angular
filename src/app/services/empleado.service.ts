import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EmpleadoService {

  constructor(private _http:HttpClient) { }

  //JSON SERVER
  addEmpleado(datos:any):Observable<any>{
    return this._http.post('http://localhost:3000/empleados',datos)
  }
  getEmpleados():Observable<any>{
    return this._http.get('http://localhost:3000/empleados');
  }
  eliminarEmpleados(id:number):Observable<any>{
    return this._http.delete(`http://localhost:3000/empleados/${id}`);
  }
  actualizarEmpleado(id:number,datos:any):Observable<any>{
    return this._http.put(`http://localhost:3000/empleados/${id}`,datos)
  }

}

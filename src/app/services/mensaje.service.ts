import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';

@Injectable({
  providedIn: 'root'
})
export class MensajeService {

  constructor(private _snackBar:MatSnackBar) { }

  abrirSnackBar(message:string,action:string="ok"){
    this._snackBar.open(message,action,{
      duration: 1000,
      verticalPosition:'top',
    });
  }
}

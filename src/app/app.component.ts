import { Component, OnInit, ViewChild} from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { AddEditEmpleadoComponent } from './components/add-edit-empleado/add-edit-empleado.component';
import { EmpleadoService } from './services/empleado.service';
import {MatPaginator, MatPaginatorModule} from '@angular/material/paginator';
import {MatSort, MatSortModule} from '@angular/material/sort';
import {MatTableDataSource, MatTableModule} from '@angular/material/table';
import { MensajeService } from './services/mensaje.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'crudEmpleados';

  displayedColumns: string[] = [
  'id',
  'nombres', 
  'apellidos',
  'correo',
  'año_nacimiento',
  'genero',
  'educacion',
  'empresa',
  'experiencia',
  'expectativa_salarial',
  'accion'
];
  dataSource!: MatTableDataSource<any>;

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  constructor(
    private _dialog:MatDialog,
    private _empleadoService:EmpleadoService,
    private _mensajeService:MensajeService){}

  ngOnInit(): void {
    this.getEmpleadosList()
  }
  
  //abrir el dialog del formulario para agregar empleado
  abrirAgregarEmpleadoForm(){
    const dialogRef = this._dialog.open(AddEditEmpleadoComponent)
    dialogRef.afterClosed().subscribe({
      next:(res)=>{
        if(res){
          //actualizar la lista de datos de la tabla
          this.getEmpleadosList();
        }
      }
    })
  }
  //listar empleados en la talba
  getEmpleadosList(){
    this._empleadoService.getEmpleados().subscribe({
      next: (res) =>{
        this.dataSource = new MatTableDataSource(res);
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
      },
      error: (err) =>{
        console.log(err)
      }
    })
  }
  //cabezera filtro de angular material
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
  //eliminar el dato por el id llamando al servicio
  eliminarEmpleado(id:number){
    if(window.confirm("Quieres eliminar al Empleado")){
      this._empleadoService.eliminarEmpleados(id).subscribe({
        next:(res)=>{
          this._mensajeService.abrirSnackBar("Empleado eliminado","Hecho")
          //actualizar la lista de datos de la tabla
          this.getEmpleadosList();
        },
        error: (err) =>{
          console.log(err)
        }
      })
    }
    
  }
  //abrir el dialog del formulario para editar empleado
  abrirEditarForm(data:any){
    const dialogRef = this._dialog.open(AddEditEmpleadoComponent,{
      data,
    });
    dialogRef.afterClosed().subscribe({
      next:(res)=>{
        if(res){
          //actualizar la tabla cuando se cierra la dialog
          this.getEmpleadosList();
        }
      }
    })
    
  }
}
